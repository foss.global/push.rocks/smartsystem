/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartsystem',
  version: '3.0.1',
  description: 'interact with the system you are running on'
}
