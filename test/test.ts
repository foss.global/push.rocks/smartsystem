import { tap, expect } from '@pushrocks/tapbundle';
import * as smartsystem from '../ts/index.js';
import { systeminformation } from '../ts/smartsystem.plugins.js';

let smartsystemInstance: smartsystem.Smartsystem;

tap.test('should create a smartsystem instance', async (tools) => {
  smartsystemInstance = new smartsystem.Smartsystem();
  expect(smartsystemInstance).toBeInstanceOf(smartsystem.Smartsystem);
});

tap.test('should state the operating system', async () => {
  expect(smartsystemInstance.cpus.length).toBeGreaterThan(0);
});

tap.test('should get systeminformation', async () => {
  console.log(await smartsystemInstance.information.networkInterfaceDefault());
});

tap.start();
